#include <iostream>

int main() {
    std::cout << "1980    " << 1*1 << "\n";
    std::cout << "1983    " << 1*4 << "\n";
    std::cout << "1986    " << 4*4 << "\n";
    std::cout << "1989    " << 16*2 << "\n";
    std::cout << "1992    " << 64*4 << "\n";
    std::cout << "1995    " << 256*4 << "\n";
    std::cout << "1998    " << 1024*4 << "\n";
    std::cout << "2003    " << 4096*4 << "\n";
}
