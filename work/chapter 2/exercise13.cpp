#include <iostream>

int main() {
    std::cout << "    *****\n";
    std::cout << "   *      *\n";
    std::cout << "  * \\    / *\n";
    std::cout << " *  o    o  *\n";
    std::cout << "*      |     *\n";
    std::cout << " *     +    *\n";
    std::cout << "  *    o   *\n";
    std::cout << "   *      *\n";
    std::cout << "    ******\n";
    std::cout << "   \"grouchy\"";
}
