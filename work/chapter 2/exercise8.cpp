#include <iostream>

int main() {
    std::cout << "6/5 = " << 6/5 << "\n";
    std::cout << "6.0/5 = " << 6.0/5 << "\n";
    std::cout << "6/5.0 = " << 6/5.0 << "\n";
    std::cout << "6.0/5.0 = " << 6.0/5.0 << "\n";
}
