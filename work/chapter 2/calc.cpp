#include <iostream>

int main() {
    std::cout << "Calculate the area of a circle but hey you can't put the actual radius in" << std::endl;
    std::cout << std::endl;
    std::cout << "Radius = " << 10 << std::endl;
    std::cout << "Area = " << (3.14*10*10) << std::endl;
    return(0);
}
