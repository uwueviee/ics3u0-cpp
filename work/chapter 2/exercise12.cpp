#include <iostream>

int main() {
    std::cout << "    *****\n";
    std::cout << "   *      *\n";
    std::cout << "  * _    _ *\n";
    std::cout << " *  o    o  *\n";
    std::cout << "*      |     *\n";
    std::cout << " *     +    *\n";
    std::cout << "  * \\____/ *\n";
    std::cout << "   *      *\n";
    std::cout << "    ******\n";
    std::cout << "   \"happy\"";
}
